﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using AppDemo.ui.activity;
using Xamarin.Essentials;

namespace AppDemo
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        TextView tvTitle;
        Button btnSubmit;
        Button btnLogin;

        int count=0;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            FloatingActionButton fab = FindViewById<FloatingActionButton>(Resource.Id.fab);
            fab.Click += FabOnClick;
            tvTitle = FindViewById<TextView>(Resource.Id.tv_title);
            btnSubmit = FindViewById<Button>(Resource.Id.btn_submit);
            btnLogin = FindViewById<Button>(Resource.Id.btn_login);
            btnSubmit.Click += SubmitOnClick;
            btnLogin.Click += LoginOnClick;
            tvTitle.Hint = "点击我会变";


            #region 设置信息
            // Device Model (SMG-950U, iPhone10,6)
            var device = DeviceInfo.Model;

            // Manufacturer (Samsung)
            var manufacturer = DeviceInfo.Manufacturer;

            // Device Name (Motz's iPhone)
            var deviceName = DeviceInfo.Name;

            // Operating System Version Number (7.0)
            var version = DeviceInfo.VersionString;

            // Platform (Android)
            var platform = DeviceInfo.Platform;

            // Idiom (Phone)
            var idiom = DeviceInfo.Idiom;

            // Device Type (Physical)
            var deviceType = DeviceInfo.DeviceType;

            #endregion


        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_settings)
            {
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        private void FabOnClick(object sender, EventArgs eventArgs)
        {
            View view = (View) sender;
            Snackbar.Make(view, "Replace with your own action", Snackbar.LengthLong)
                .SetAction("Action", (Android.Views.View.IOnClickListener)null).Show();
        }


        private void SubmitOnClick(object sender, EventArgs eventArgs)
        {
            View view = (View)sender;
            count++;
            tvTitle.Text = "第"+count+"次点击我";

        }


        private void LoginOnClick(object sender, EventArgs eventArgs)
        {
            View view = (View)sender;
            Intent mIntent = new Intent(this,typeof(LoginActivity));
            StartActivity(mIntent);
        }
    }
}

