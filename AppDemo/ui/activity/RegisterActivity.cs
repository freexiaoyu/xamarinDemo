﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Com.Bumptech.Glide;

namespace AppDemo.ui.activity
{
    class RegisterActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_register);
            ImageView ivPhoto = FindViewById<ImageView>(Resource.Id.iv_photo);
            //加载网络图片
            Glide.With(this).Load("").Into(ivPhoto);

        }
    }
}