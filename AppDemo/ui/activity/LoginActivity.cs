﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using AppDemo.utils;
using Com.Bumptech.Glide;
using Java.Net;
using RestSharp;

namespace AppDemo.ui.activity
{
    [Activity(ParentActivity = typeof(MainActivity), Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar")]
    class LoginActivity : AppCompatActivity
    {
        string url = "http://abc.abc.com/";

        TextView tvJson;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_login);
            ImageView ivPhoto = FindViewById<ImageView>(Resource.Id.iv_photo);
            //加载网络图片
            Glide.With(this).Load("https://www.cnblogs.com/images/logo_small.gif").Into(ivPhoto);
            tvJson = FindViewById<TextView>(Resource.Id.tv_json);
            Button btnGet = FindViewById<Button>(Resource.Id.btn_get);
            Button btnPost = FindViewById<Button>(Resource.Id.btn_post);
            btnGet.Click += (sender, e) => {
                RestJsonGet();
            };
            btnGet.Click += (sender, e) => {
                RestJsonPost();
            };
        }

        /// <summary>
        /// Get请求
        /// </summary>
        private void RestJsonGet()
        {
            var client = new RestSharp.RestClient(url);

            var requestGet = new RestRequest("api/DailyWordApi.ashx?ver={ver}&action={action}", Method.GET);
            requestGet.AddUrlSegment("ver", "1");
            requestGet.AddUrlSegment("action", "DailyWord");
            IRestResponse response = client.Execute(requestGet);
            var contentGet = response.Content;
            tvJson.Text = contentGet;

        }

        /// <summary>
        /// POST请求
        /// </summary>
        private void RestJsonPost()
        {
            Dictionary<string,string> dic = new Dictionary<string, string>();
            dic.Add("ver", "1");
            dic.Add("action", "DailyWord");

            var client = new RestSharp.RestClient(url);
            var requestGet = new RestRequest("api/DailyWordApi.ashx", Method.POST); 
            requestGet.AddQueryParameter("ver", "1");
            requestGet.AddQueryParameter("action", "DailyWord");
            IRestResponse response = client.Execute(requestGet);
            var contentGet = response.Content;
            tvJson.Text = contentGet;

        }


    }
}